# Spyder Containers

This is a sample Docker container to run a Spyder external kernel.

In this example the Docker image is based on Python 3.11 but you can choose the one you like.

## Getting started
To pull the repo and the necessary code:

```shell
$ git clone https://gitlab.com/noze/spyder_containers.git 
$ cd spyder_containers
```

To build the Docker image 
```shell
$ docker compose build
```

To launch the Docker compose
```shell
$ docker compose up
```

## Attach the kernel
To attach the kernel in Spyder go to Console --> Connect to an existing kernel menu and simply select the kernel.json file from this repo as Connection file (this sample dows not use a remote kernel).

## Open issues

For now the kernel ports are fixed (45551, 45552, 45553, 45554, 45555) and may conflict with other applications on your machine.
